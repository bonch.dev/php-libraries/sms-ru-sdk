<?php


namespace BonchDev\SmsRuSDK;


class SendException extends \Exception
{
    private $response;
    /**
     * @var array
     */
    private $jsonResponse;
    /**
     * @var array
     */
    private $failedMessages;

    public function __construct(
        $response,
        array $jsonResponse,
        array $failedMessages
    )
    {
        parent::__construct(
            'Не все сообщения были доставлены...',
            $jsonResponse['status_code']
        );

        $this->response = $response;
        $this->jsonResponse = $jsonResponse;
        $this->failedMessages = $failedMessages;
    }

    public function getResponse()
    {
        return $this->response;
    }

    public function getJsonResponse()
    {
        return $this->jsonResponse;
    }

    public function getFailedMessages()
    {
        return $this->failedMessages;
    }
}